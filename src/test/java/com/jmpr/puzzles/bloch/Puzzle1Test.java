package com.jmpr.puzzles.bloch;

import org.junit.Assert;
import org.junit.Test;

public class Puzzle1Test {
    @Test
    public void testPuzzle1() throws Exception {
        Assert.assertTrue(Puzzle1.isOdd(1));
    }
}
