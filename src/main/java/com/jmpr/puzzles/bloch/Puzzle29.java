package com.jmpr.puzzles.bloch;

public class Puzzle29 {
    public static void test() {
        short count = -1;
        while (count != 0)
            count >>>= 1;
    }
}