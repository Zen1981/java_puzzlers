package com.jmpr.puzzles.bloch;

public class Puzzle8 {
    public static void test() {
        char x = 'X';
        int i = 0;
        System.out.print(true ? x : 0);
        System.out.print(false ? i : x);
    }
}
