package com.jmpr.puzzles.bloch;

public class Puzzle14 {
    public static void test() {
        // \u0022 is the Unicode for double quote (")
        System.out.println("a\u0022.length() + \u0022b".length());
    }
}