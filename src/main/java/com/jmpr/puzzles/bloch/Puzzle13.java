package com.jmpr.puzzles.bloch;

public class Puzzle13 {
    public static void test() {
        final String pig = "length: 10";
        final String dog = "length: " + pig.length();
        System.out.println("Animals are equal: " + pig == dog);
    }
}