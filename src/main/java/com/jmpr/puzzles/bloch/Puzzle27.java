package com.jmpr.puzzles.bloch;

public class Puzzle27 {
    public static void test() {
        int count = 0;
        while (-1 << count != 0)
            count++;
        System.out.println(count);
    }
}