package com.jmpr.puzzles.bloch;

public class Puzzle1 {
    public static boolean isOdd(int i) {
        return i % 2 == 1;
    }
}
