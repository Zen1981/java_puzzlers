package com.jmpr.puzzles.bloch;

import java.util.Random;

public class Puzzle23 {
    private static Random rnd = new Random();

    public static void test() {
        StringBuffer word = null;

        switch (rnd.nextInt(2)) {
            case 1: word = new StringBuffer('P');
            case 2: word = new StringBuffer('G');
            default: word = new StringBuffer('M');
        }

        word.append('a');
        word.append('i');
        word.append('n');

        System.out.println(word);
    }
}