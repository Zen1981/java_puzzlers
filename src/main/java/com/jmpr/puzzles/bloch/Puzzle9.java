package com.jmpr.puzzles.bloch;

public class Puzzle9 {
    public static void test() {
        short x = 0;
        int i = 123456;

        // Don't compile
        // x = x + i;

        System.out.print(x += i);
    }
}
