package com.jmpr.puzzles.bloch;

public class Puzzle7 {
    public static void test() {
        int x = 1984;	// (0x7c0)
        int y = 2001;	// (0x7d1)
        x ^= y ^= x ^= y;
        System.out.println("x = " + x + "; y =" + y);
    }
}
