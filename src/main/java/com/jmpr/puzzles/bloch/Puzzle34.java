package com.jmpr.puzzles.bloch;

public class Puzzle34 {
    public static void test() {
        final int inicio = 2000000000;
        int contador = 0;
        for (float i = inicio; i < inicio + 50; i++)
            contador ++;
        System.out.println(contador);
    }
}