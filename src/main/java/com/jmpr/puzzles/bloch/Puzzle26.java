package com.jmpr.puzzles.bloch;

public class Puzzle26 {
    public static final int END = Integer.MAX_VALUE;
    public static final int START = END - 100;

    public static void test() {
        int count = 0;
        for (int i = START; i <= END; i++)
            count++;
        System.out.println(count);
    }
}