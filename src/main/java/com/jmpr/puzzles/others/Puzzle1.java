package com.jmpr.puzzles.others;

/**
 * @author BBVA Open Platform Development Team
 */
public class Puzzle1 {
    public static void test(){
        long startTime = System.currentTimeMillis();
        StringBuffer sb = new StringBuffer("anyforum");
        for (int i=0; i<1000000; i++){
            sb.append("india");
        }
        System.out.println("Time taken by StringBuffer: " + (System.currentTimeMillis() - startTime) + "ms");
        startTime = System.currentTimeMillis();
        StringBuilder sb2 = new StringBuilder("anyforum");
        for (int i=0; i<1000000; i++){
            sb2.append("india");
        }
        System.out.println("Time taken by StringBuilder: " + (System.currentTimeMillis() - startTime) + "ms");
    }
}
